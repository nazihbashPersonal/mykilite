//
//  MykiLiteTests.swift
//  MykiLiteTests
//
//  Created by Awwad on 8/16/19.
//  Copyright © 2019 myki. All rights reserved.
//

import XCTest
@testable import MykiLite

class MykiLiteTests: XCTestCase {

    var listViewModel: PasswordsViewModel!
    var addViewModel: AddPasswordViewModel!
    
    override func setUp() {
        super.setUp()
        
        listViewModel = PasswordsViewModel()
        addViewModel = AddPasswordViewModel()
        
        for index in 0..<addViewModel.fields.count {
//            if addViewModel.fields[index].type == .nickname {
//                continue
//            }
            addViewModel.fields[index].value = randomString(length: 8)
        }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        listViewModel = nil
        addViewModel = nil
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    private func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    
    func testFormIsValid() {
        XCTAssertEqual(addViewModel.isFormValid, true, "Form invalid")
    }
    
    func testDidSave() {
        var password = Password()
        addViewModel.fields.forEach({ password.mapField($0) })
        
        database.createOrUpdate(model: password, with: PasswordObject.init)
        
        listViewModel.loadData(searchText: password.nickname)
        XCTAssert(listViewModel.numberOfCells > 0, "Save or fetch password was unsuccessfull")

    }
}
