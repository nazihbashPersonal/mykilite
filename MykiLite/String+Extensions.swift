//
//  String+Extensions.swift
//  MykiLite
//
//  Created by nazih bash on 11/30/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    var isValidURL: Bool {
        let regex = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        return test.evaluate(with: self)
    }
    
    var logoURL: URL? {
        return URL(string: Constants.clearbitURL + self)
    }
    
    var sha1: String {
        let data = Data(self.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
