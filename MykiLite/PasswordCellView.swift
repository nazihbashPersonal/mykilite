//
//  PasswordCellView.swift
//  MykiLite
//
//  Created by Awwad on 8/13/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

class PasswordCellView: UITableViewCell {
    
    private var itemBackView = UIView()
    private var nicknameLabel = UILabel()
    private var emailLabel = UILabel()
    private var itemImageView = UIImageView()
    
    var password: Password? {
        didSet {
            guard let password = password else { return }
            nicknameLabel.text = password.nickname
            emailLabel.text = password.url.isEmpty ? password.username : password.url
            if password.url.isValidURL {
                itemImageView.kf.setImage(with: password.url.logoURL, placeholder: UIImage(named: "default"))
            } else {
                itemImageView.image = UIImage(named: "default")
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        
        itemBackView.layer.masksToBounds = true
        itemBackView.backgroundColor = .navBar
        itemBackView.layer.borderWidth = 1
        itemBackView.layer.borderColor = UIColor.clear.cgColor
        itemBackView.layer.cornerRadius = 3
        
        itemImageView.contentMode = .scaleAspectFit
        itemImageView.layer.masksToBounds = true
        itemImageView.layer.borderWidth = 1
        itemImageView.layer.borderColor = UIColor.clear.cgColor
        
        nicknameLabel.adjustsFontSizeToFitWidth = true
        nicknameLabel.textColor = .white
        nicknameLabel.font = UIFont.boldSystemFont(ofSize: 17)
        
        emailLabel.adjustsFontSizeToFitWidth = true
        emailLabel.textColor = .grayG
        emailLabel.font = UIFont.systemFont(ofSize: 13)
        
        self.contentView.addSubview(itemBackView)
        itemBackView.addSubview(itemImageView)
        itemBackView.addSubview(nicknameLabel)
        itemBackView.addSubview(emailLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        itemBackView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        nicknameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            itemBackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            itemBackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            itemBackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            itemBackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            itemImageView.leadingAnchor.constraint(equalTo: itemBackView.leadingAnchor, constant: 10),
            itemImageView.topAnchor.constraint(equalTo: itemBackView.topAnchor, constant: 10),
            itemImageView.heightAnchor.constraint(equalTo: itemBackView.heightAnchor, constant: -20),
            itemImageView.widthAnchor.constraint(equalTo: itemBackView.heightAnchor, constant: -20),
            
            nicknameLabel.leadingAnchor.constraint(equalTo: itemImageView.trailingAnchor, constant: 8),
            nicknameLabel.topAnchor.constraint(equalTo: itemImageView.topAnchor, constant: 10),
            nicknameLabel.heightAnchor.constraint(equalToConstant: 20),
            nicknameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            
            emailLabel.leadingAnchor.constraint(equalTo: itemImageView.trailingAnchor, constant: 8),
            emailLabel.topAnchor.constraint(equalTo: nicknameLabel.bottomAnchor),
            emailLabel.heightAnchor.constraint(equalToConstant: 14),
            emailLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        ])
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        itemImageView.layer.cornerRadius = itemImageView.frame.height / 2
    }
    
}

