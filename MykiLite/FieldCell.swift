//
//  FieldCell.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation
import UIKit

protocol FieldCellDelegate: class {
    func copyText(_ text: String?)
}
class FieldCell: UITableViewCell {
    
    private var infoBackgroundView = UIView()
    var titleLabel = UILabel()
    var detailTextField = UITextField()
    var copyButton = UIButton(type: .system)
    weak var delegate: FieldCellDelegate?
    
    var value: String = "" {
        didSet {
            if shouldHoldToReveal {
                detailTextField.text = "Hold to reveal item"
            } else {
                detailTextField.text = value
            }
        }
    }
    
    var shouldHoldToReveal: Bool = false
    var editingEnabled: Bool = false {
        didSet {
            detailTextField.isUserInteractionEnabled = editingEnabled
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        addLongGesture()
        self.backgroundColor = UIColor.clear
        infoBackgroundView.layer.masksToBounds = true
        infoBackgroundView.layer.borderWidth = 1
        infoBackgroundView.layer.borderColor = UIColor.clear.cgColor
        infoBackgroundView.backgroundColor = .darkGray
        
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textColor = .grayG
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        detailTextField.adjustsFontSizeToFitWidth = true
        detailTextField.textColor = .white
        detailTextField.font = UIFont.systemFont(ofSize: 16)
        detailTextField.autocorrectionType = .no
        detailTextField.tintColor = .mykiGreen
        detailTextField.autocapitalizationType = .none
        detailTextField.keyboardAppearance = .dark
        
        copyButton.tintColor = .mykiGreen
        copyButton.setTitle("COPY", for: .normal)
        copyButton.titleLabel?.font = .boldSystemFont(ofSize: 15)
        copyButton.addTarget(self, action: #selector(copyButtonTapped(_:)), for: .touchUpInside)
        
        self.contentView.addSubview(infoBackgroundView)
        infoBackgroundView.addSubview(titleLabel)
        infoBackgroundView.addSubview(detailTextField)
        infoBackgroundView.addSubview(copyButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        infoBackgroundView.layer.cornerRadius = 3.0
        infoBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        detailTextField.translatesAutoresizingMaskIntoConstraints = false
        copyButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            infoBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            infoBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            infoBackgroundView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            infoBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            titleLabel.leadingAnchor.constraint(equalTo: infoBackgroundView.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: infoBackgroundView.trailingAnchor, constant: -10),
            titleLabel.topAnchor.constraint(equalTo: infoBackgroundView.topAnchor, constant: 10),
            titleLabel.heightAnchor.constraint(equalToConstant: 18),
            
            detailTextField.leadingAnchor.constraint(equalTo: infoBackgroundView.leadingAnchor, constant: 10),
            detailTextField.trailingAnchor.constraint(equalTo: infoBackgroundView.trailingAnchor, constant: -10),
            detailTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            detailTextField.heightAnchor.constraint(equalToConstant: 18),
            
            copyButton.trailingAnchor.constraint(equalTo: infoBackgroundView.trailingAnchor, constant: -20),
            copyButton.centerYAnchor.constraint(equalTo: infoBackgroundView.centerYAnchor),
        ])
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
    }
    
    @objc private func copyButtonTapped(_ sender: UIButton) {
        delegate?.copyText(value)
    }
    
    private func addLongGesture() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(showPassword))
        addGestureRecognizer(longPressRecognizer)
    }
    
    @objc private func showPassword() {
        if shouldHoldToReveal {
            detailTextField.text = value
        }
    }
}
