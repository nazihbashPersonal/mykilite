//
//  Field.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

enum FieldType: CaseIterable {
    
    case header
    case nickname
    case username
    case password
    case website
    
    var key: String {
        switch self {
        case .nickname:
            return "nickname"
        case .username:
            return "username"
        case .password:
            return "password"
        case .website:
            return "url"
        case .header:
            return ""
        }
    }
    
    var title: String {
        switch self {
        case .nickname:
            return "nickname".localized
        case .username:
            return "username".localized
        case .password:
            return "password".localized
        case .website:
            return "website".localized
        case .header:
            return ""
        }
    }
    
    var isSecure: Bool {
        switch self {
        case .password:
            return true
        default:
            return false
        }
    }
}

struct Field {
    var type: FieldType
    var value = ""
    
    init(type: FieldType, password: Password) {
        switch type {
        case .header:
            self.init(type: type)
        case .nickname:
            self.init(type: type, value: password.nickname)
        case .username:
            self.init(type: type, value: password.username)
        case .password:
            self.init(type: type, value: password.password)
        case .website:
            self.init(type: type, value: password.url)
        }
    }
    
    init(type: FieldType, value: String? = nil) {
        self.type = type
        if let value = value {
            self.value = value
        }
    }
}
