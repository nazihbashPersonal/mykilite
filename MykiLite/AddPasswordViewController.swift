//
//  AddPasswordViewController.swift
//  MykiLite
//
//  Created by Awwad on 8/13/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

class AddPasswordViewController: UITableViewController {
    
    let viewModel: AddPasswordViewModel
    let isDetails: Bool
    var websiteValue: String?
    
    var editingEnabled = false {
        didSet {
            if editingEnabled {
                navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelEditing))
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(savePassword))
            } else {
                navigationItem.leftBarButtonItem = nil
                navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(startEditing))
            }
            tableView.reloadData()
        }
    }
    
    init(password: Password? = nil) {
        if let password = password {
            isDetails = true
            viewModel = AddPasswordViewModel(password: password)
            websiteValue = password.url
        } else {
            isDetails = false
            viewModel = AddPasswordViewModel()
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        configureNavBar()
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        guard isDetails else {
            return
        }
        navigationController?.setToolbarHidden(false, animated: true)
        navigationController?.toolbar.barTintColor = .navBar
        navigationController?.toolbar.tintColor = .mykiGreen
        
        let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonTapped))
        navigationController?.toolbar.items = [deleteButton]
        navigationController?.toolbar.sizeToFit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        guard isDetails else {
            return
        }
        navigationController?.isToolbarHidden = true
        navigationController?.toolbar.items = nil
    }
    
    private func configureNavBar() {
        self.title = isDetails ? "" : "add_password".localized
        self.navigationItem.largeTitleDisplayMode = .never
        if isDetails {
            editingEnabled = false
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(savePassword))
        }
    }
    
    private func configureView() {
        view.backgroundColor = .black
        tableView.separatorStyle = .none
        if isDetails {
            tableView.register(PasswordDetailsHeaderCell.self, forCellReuseIdentifier: "headerCell")
        } else {
            tableView.register(PasswordHeaderCellView.self, forCellReuseIdentifier: "headerCell")
        }
        tableView.register(FieldCell.self, forCellReuseIdentifier: "detailCell")
    }
    
    @objc private func savePassword() {
        guard viewModel.isFormValid else {
            let alertController = UIAlertController(title: "error".localized, message: "nickname_mandatory".localized, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok".localized, style: .default)
            alertController.addAction(okAction)
            present(alertController, animated: true)
            
            return
        }
        viewModel.savePassword()
    }
    
    @objc func textFieldChanged(_ textField: UITextField) {
        if let text = textField.text {
            viewModel.fields[textField.tag].value = text
        }
    }
    
    @objc func textFieldDidEnd(_ textField: UITextField) {
        let field = viewModel.fields[textField.tag]
        if field.type == .website {
            websiteValue = field.value
            tableView.reloadData()
        }
    }
    
    @objc private func startEditing() {
        editingEnabled = true
    }
    
    @objc private func cancelEditing() {
        editingEnabled = false
    }
    
    @objc private func deleteButtonTapped() {
        let alertController = UIAlertController(title: "remove_account".localized, message: "select_delete_option".localized, preferredStyle: .actionSheet)
        let okAction = UIAlertAction(title: "delete_account".localized, style: .destructive, handler: { [weak self] action in
            self?.viewModel.deletePassword()
            self?.navigationController?.popViewController(animated: true)
        })
        let cancelAction = UIAlertAction(title: "keep_account".localized, style: .cancel)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
}
extension AddPasswordViewController: AddPasswordViewModelDelegate {
    func showErrorMessage(_ message: String) {
        let alertController = UIAlertController(title: "error".localized, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok".localized, style: .default)
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
    
    func didSavePassword() {
        if isDetails {
            cancelEditing()
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
}
