//
//  PasswordService.swift
//  MykiLite
//
//  Created by nazih bash on 12/1/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

final class PasswordService {
    
    static let shared = PasswordService()
    private let defaultSession = URLSession(configuration: .default)
    private let baseURL = "https://api.pwnedpasswords.com/range"
    
    func checkLeakage(password: String, completion: @escaping (Result<Bool, Error>) -> ()) {
        if var url = URL(string: baseURL) {
            
            let passwordHash = password.sha1
            url.appendPathComponent(String(passwordHash.prefix(5)))
            
            let dataTask = defaultSession.dataTask(with: url) { [weak self] data, response, error in
                guard self != nil else { return }
                
                if let error = error {
                    completion(.failure(error))
                    
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    
                    let responseString = String(data: data, encoding: .utf8)
                    let hashArray = responseString?.split(separator: "\r\n") ?? []
                    
                    let isLeaked = !hashArray.filter({ $0.contains(passwordHash) }).isEmpty
                    
                    DispatchQueue.main.async {
                        completion(.success(isLeaked))
                    }
                }
            }
            dataTask.resume()
        }

    }
}
