//
//  UIView+Extensions.swift
//  MykiLite
//
//  Created by nazih bash on 11/29/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

extension UIView {
    func dock() {
        dockInSuperView(leading: true, trailing: true, top: true, bottom: true)
    }
    
    func dockInSuperView(leading: Bool = false, trailing: Bool = false, top: Bool = false, bottom: Bool = false, constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { return }
        
        if leading {
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant).isActive = true
        }
        if trailing {
            superview.trailingAnchor.constraint(equalTo: trailingAnchor, constant: constant).isActive = true
        }
        if top {
            topAnchor.constraint(equalTo: superview.topAnchor, constant: constant).isActive = true
        }
        if bottom {
            superview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: constant).isActive = true
        }
    }
    
    func centerInSuperView() {
        translatesAutoresizingMaskIntoConstraints = false
        guard let superview = superview else { return }
        
        centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
        centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
    }
}
