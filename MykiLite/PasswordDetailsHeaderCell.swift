//
//  PasswordDetailsHeaderCell.swift
//  MykiLite
//
//  Created by nazih bash on 11/29/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit
final class PasswordDetailsHeaderCell: UITableViewCell {
    var itemBackgroundView = UIView()
    var itemImageView = UIImageView()
    var nicknameLabel = UILabel()
    var usernameLabel = UILabel()
    private var labelsContainer = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        
        itemBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        itemBackgroundView.backgroundColor = .darkGray
        itemBackgroundView.layer.borderWidth = 1
        itemBackgroundView.layer.borderColor = UIColor.darkGray.cgColor
        itemBackgroundView.layer.cornerRadius = 5
        
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.layer.masksToBounds = true
        itemImageView.layer.borderWidth = 1
        itemImageView.layer.borderColor = UIColor.clear.cgColor
        itemImageView.image = UIImage(named: "default")
        
        nicknameLabel.translatesAutoresizingMaskIntoConstraints = false
        nicknameLabel.adjustsFontSizeToFitWidth = true
        nicknameLabel.textColor = .white
        nicknameLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameLabel.adjustsFontSizeToFitWidth = true
        usernameLabel.textColor = .white
        usernameLabel.font = UIFont.systemFont(ofSize: 16)
        
        labelsContainer.translatesAutoresizingMaskIntoConstraints = false
        labelsContainer.backgroundColor = .clear
        
        labelsContainer.addSubview(nicknameLabel)
        labelsContainer.addSubview(usernameLabel)
        
        self.contentView.addSubview(itemBackgroundView)
        itemBackgroundView.addSubview(itemImageView)
        itemBackgroundView.addSubview(labelsContainer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let constraints: [NSLayoutConstraint] = [
            itemBackgroundView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            itemBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            itemBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            itemBackgroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            itemImageView.leadingAnchor.constraint(equalTo: itemBackgroundView.leadingAnchor, constant: 10),
            itemImageView.centerYAnchor.constraint(equalTo: itemBackgroundView.centerYAnchor),
            itemImageView.widthAnchor.constraint(equalTo: itemBackgroundView.heightAnchor, multiplier: 0.6),
            itemImageView.heightAnchor.constraint(equalTo: itemBackgroundView.heightAnchor, multiplier: 0.6),
            
            labelsContainer.centerYAnchor.constraint(equalTo: itemBackgroundView.centerYAnchor),
            labelsContainer.leadingAnchor.constraint(equalTo: itemImageView.trailingAnchor, constant: 8),
            labelsContainer.trailingAnchor.constraint(equalTo: itemBackgroundView.trailingAnchor, constant: 0),
        ]
        NSLayoutConstraint.activate(constraints)

        nicknameLabel.dockInSuperView(leading: true, trailing: true, top: true)
        usernameLabel.dockInSuperView(leading: true, trailing: true, bottom: true)
        usernameLabel.topAnchor.constraint(equalTo: nicknameLabel.bottomAnchor, constant: 2).isActive = true
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        itemImageView.layer.cornerRadius = itemImageView.frame.height / 2
    }
}
