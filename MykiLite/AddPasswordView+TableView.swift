//
//  AddPasswordView+TableView.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

extension AddPasswordViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let field = viewModel.fields[indexPath.row]
        switch field.type {
        case .header:
            return getHeaderCell(indexPath: indexPath)
        default:
            return getFieldCell(indexPath: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let field = viewModel.fields[indexPath.row]
        switch field.type {
        case .header:
            return 120
        default:
            return 80
        }
    }
    
    private func getHeaderCell(indexPath: IndexPath) -> UITableViewCell {
        if isDetails {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! PasswordDetailsHeaderCell
            cell.selectionStyle = .none
            cell.nicknameLabel.text = viewModel.password?.nickname
            cell.usernameLabel.text = viewModel.password?.username
            
            cell.itemBackgroundView.backgroundColor = .navBar
            if websiteValue?.isValidURL ?? false {
                cell.itemImageView.kf.setImage(with: websiteValue?.logoURL, placeholder: UIImage(named: "default"), options: [], progressBlock: nil, completionHandler: { result in
                    switch result {
                    case .success(let image):
                        cell.itemBackgroundView.backgroundColor = image.image.dominantColor ?? .navBar
                    default:
                        break
                    }
                })
            } else {
                cell.itemImageView.image = UIImage(named: "default")
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! PasswordHeaderCellView
            cell.selectionStyle = .none
            
            cell.itemBackgroundView.backgroundColor = .navBar
            if websiteValue?.isValidURL ?? false {
                cell.itemImageView.kf.setImage(with: websiteValue?.logoURL, placeholder: UIImage(named: "default"), options: [], progressBlock: nil, completionHandler: { result in
                    switch result {
                    case .success(let image):
                        cell.itemBackgroundView.backgroundColor = image.image.dominantColor ?? .navBar
                    default:
                        break
                    }
                })
            } else {
                cell.itemImageView.image = UIImage(named: "default")
            }
            return cell
        }
    }
    
    private func getFieldCell(indexPath: IndexPath) -> UITableViewCell {
        let field = viewModel.fields[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! FieldCell
        cell.delegate = self
        cell.titleLabel.text = field.type.title
        
        if isDetails {
            cell.shouldHoldToReveal = field.type.isSecure && !editingEnabled
            cell.editingEnabled = editingEnabled
        } else {
            cell.shouldHoldToReveal = false
            cell.detailTextField.isSecureTextEntry = field.type.isSecure
            cell.editingEnabled = true
        }
        if editingEnabled || field.value.isEmpty || !isDetails {
            cell.copyButton.isHidden = true
        } else {
            cell.copyButton.isHidden = field.type != .username && field.type != .password
        }
        cell.value = field.value
        
        cell.detailTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
        cell.detailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        cell.detailTextField.tag = indexPath.row
        
        return cell
    }
}
extension AddPasswordViewController: FieldCellDelegate {
    
    func copyText(_ text: String?) {
        UIPasteboard.general.string = text
    }
}
