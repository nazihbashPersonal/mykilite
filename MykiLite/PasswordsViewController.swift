//
//  PasswordsViewController.swift
//  MykiLite
//
//  Created by Awwad on 8/13/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

class PasswordsViewController: UITableViewController {
    
    let viewModel: PasswordsViewModel
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "search_placeholder".localized
        navigationItem.searchController = searchController
        definesPresentationContext = true

        return searchController
    }()
    
    init() {
        viewModel = PasswordsViewModel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.loadData(searchText: nil)
        configureSearchController()
        configureNavBar()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadData(searchText: searchController.searchBar.text)
        tableView.reloadData()
    }
    
    @objc private func addPassword() {
        let controller = AddPasswordViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func configureSearchController() {
        searchController.searchResultsUpdater = self
    }
    
    private func configureNavBar() {
        self.title = "passwords".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPassword))
    }
    
    private func configureView() {
        view.backgroundColor = .darkGray
        tableView.separatorStyle = .none
        tableView.indicatorStyle = .white
        tableView.register(PasswordCellView.self, forCellReuseIdentifier: "cell")
    }
}

extension PasswordsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        viewModel.loadData(searchText: searchBar.text)
        tableView.reloadData()
    }
}
