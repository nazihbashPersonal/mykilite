//
//  Password.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

struct Password {
    var uuid = UUID().uuidString.lowercased()
    var nickname: String = ""
    var username: String = ""
    var password: String = ""
    var url: String = ""
}

extension Password {
    init(object: PasswordObject) {
        self.uuid = object.uuid
        self.nickname = object.nickname
        self.username = object.username
        self.password = object.password
        self.url = object.url
    }
    
    mutating func mapField(_ field: Field) {
        switch field.type {
        case .nickname:
            nickname = field.value
        case .username:
            username = field.value
        case .password:
            password = field.value
        case .website:
            url = field.value
        default:
            break
        }
    }
}
