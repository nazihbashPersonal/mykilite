//
//  PasswordsViewModel.swift
//  MykiLite
//
//  Created by Awwad on 8/13/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

class PasswordsViewModel {
    
    private var passwords: [Password] = []
    
    func loadData(searchText: String?) {
        if let searchText = searchText, !searchText.isEmpty {
            let predicate = NSPredicate(format: "nickname contains[c] %@ OR username contains[c] %@", searchText.lowercased(), searchText.lowercased())
            let request = FetchRequest<[Password], PasswordObject>(predicate: predicate, sortDescriptors: [], transformer: { $0.map(Password.init) })
            passwords = database.fetch(with: request)
        } else {
            passwords = database.fetch(with: Password.all)
        }
    }
    
    var numberOfCells: Int {
        return passwords.count
    }
    
    func getPassword(row: Int) -> Password {
        return passwords[row]
    }
    
    
}

