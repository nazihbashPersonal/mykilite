//
//  AddPasswordViewModel.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

protocol AddPasswordViewModelDelegate {
    func showErrorMessage(_ message: String)
    func didSavePassword()
}
class AddPasswordViewModel {
    
    var password: Password?
    var fields: [Field] = []
    var delegate: AddPasswordViewModelDelegate?
    
    var numberOfCells: Int {
        return fields.count
    }
    
    init(password: Password? = nil) {
        if let password = password {
            self.password = password
            fields = FieldType.allCases.map { Field(type: $0, password: password) }
        } else {
            fields = FieldType.allCases.map { Field(type: $0) }
        }
    }
    
    var isFormValid: Bool {
        if let nickname = fields.first(where: { $0.type == .nickname }),
            !nickname.value.isEmpty {
            return true
        }
        return false
    }
    
    func savePassword() {
        guard let passwordText = fields.first(where: { $0.type == .password })?.value else { return }
        
        PasswordService.shared.checkLeakage(password: passwordText) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let isLeaked):
                if isLeaked {
                    self.delegate?.showErrorMessage("The password you entered is leaked.")
                    
                } else {
                    if self.password == nil {
                        self.password = Password()
                    }
                    self.fields.forEach { self.password?.mapField($0) }

                    if let password = self.password {
                        database.createOrUpdate(model: password, with: PasswordObject.init)
                        self.delegate?.didSavePassword()
                    }
                }
            case .failure(_):
                self.delegate?.showErrorMessage("Error occurred, please try again.")
            }
        }
    }
    
    func deletePassword() {
        guard let password = password else { return }
        database.delete(type: PasswordObject.self, with: password.uuid)
    }
}

